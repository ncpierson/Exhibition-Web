import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import TerminalInput from '../TerminalInput';

class Home extends Component {
  consentStrings = ['yes, i consent', 'yes i consent'];

  constructor() {
    super();

    this.state = {
      consentValue: ''
    };

    this.handleConsentInputChange = this.handleConsentInputChange.bind(this);
  }

  handleConsentInputChange(e) {
    this.setState({
      consentValue: e.target.value
    });
  }

  render() {
    const { consentValue } = this.state;

    if (this.consentStrings.includes(consentValue.toLowerCase())) {
      return <Redirect push to="/exhibits/networth" />;
    }

    return (
      <main>
        <section className="section">
          <h1 className="section__title">Hi</h1>
          <p className="section__subtitle">
            My name is Nick Pierson. I am on a quest to simplify, organize, and
            share my life.
          </p>
          <h2 className="section__label">What?</h2>
          <p className="section__text">
            This is a website that tracks my net worth, happiness, and things
            that I own and exposes this information to anyone that wants to
            look. In the future, I would like to expose even more – like my full
            budget.
          </p>
          <h2 className="section__label">Why?</h2>
          <p className="section__text">
            For two reasons. The first is that I would like to track this
            information myself and this is motivation to do so. Secondly, I want
            to expose this information as an experiment in transparency. I see
            no reason to keep it a secret.
          </p>
          <h2 className="section__label">Who?</h2>
          <p className="section__text">
            Hi, I'm Nick. I have a{' '}
            <a href="https://twitter.com/NickOnTheWeb">twitter</a> and a{' '}
            <a href="https://blog.nickpierson.name">blog</a>. I'm not a
            celebrity or anything – just an adventurous developer.
          </p>
        </section>
        <section className="section section--bordered">
          <p className="section__subtitle">
            I want to share personal information with you. Do you consent?
          </p>
          <TerminalInput
            onChange={this.handleConsentInputChange}
            placeholder="Yes, I consent"
            value={consentValue}
          />
        </section>
      </main>
    );
  }
}

export default Home;
