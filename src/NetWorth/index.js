import React, { Component } from 'react';

class NetWorth extends Component {
  render() {
    return (
      <main>
        <section className="exhibit">
          <div className="exhibit__number">
            <h1 className="exhibit__title">Net Worth</h1>
            <span>$55,790</span>
          </div>
        </section>
        <section className="section">
          <h2 className="section__title">Info</h2>
          <p className="section__text">
            The first <em>exhibit</em> is my net worth because it is one of the
            easiest ways to quantify my life and I wanted to start with
            something easy. Additionally, I have always disliked the taboo
            around wealth and I want to be transparent about my own wealth. One
            day I would like to show my entire budget including itemized income,
            expenses, and any charitable donations.
          </p>
          <p className="section__text">
            My net worth is sourced from a variety of data sources including
            Ally Bank, Vanguard, and cryptocurrencies Bitcoin, BCash, Ethereum,
            and Ripple. I will be using{' '}
            <a href="https://plaid.com">Plaid API</a> and the{' '}
            <a href="https://coinmarketcap.com/api/">CoinMarketCap API</a> in
            the future for hourly updates.
          </p>
        </section>
      </main>
    );
  }
}

export default NetWorth;
