import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home/';
import NetWorth from './NetWorth/';
import Happiness from './Happiness/';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/exhibits/networth" component={NetWorth} />
          <Route exact path="/exhibits/happiness" component={Happiness} />
        </Switch>
      </div>
    );
  }
}

export default App;
