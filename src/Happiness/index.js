import React, { Component } from 'react';

class Happiness extends Component {
  render() {
    return (
      <main>
        <section className="exhibit">
          <div className="exhibit__number">
            <h1 className="exhibit__title">Happiness</h1>
            <span>7.8</span>
          </div>
        </section>
        <section className="section">
          <h2 className="section__title">Info</h2>
          <p className="section__text">
            I want to track my happiness in order to spot if there are any
            trends over time or if my happiness correlates with any other aspect
            of my life. I have never tried to track my happiness before, so I am
            not sure if I will find any value in this exercise.
          </p>
          <p className="section__text">
            My happiness rating is the average of the scores for the last seven
            days. I would eventually like to average this across months or even
            years.
          </p>
        </section>
      </main>
    );
  }
}

export default Happiness;
