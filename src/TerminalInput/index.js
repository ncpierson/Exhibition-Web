import React, { Component } from 'react';

import './terminal.css';

class TerminalInput extends Component {
  constructor() {
    super();

    this.focusTextInput = this.focusTextInput.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  focusTextInput() {
    this.input.focus();
  }

  handleKeyDown(event) {
    switch (event.keyCode) {
      case 36: // home
      case 37: // left arrow
      case 38: // up arrow
      case 39: // right arrow
      case 40: // down arrow
        event.preventDefault();
        break;
      default:
    }
  }

  render() {
    const { onChange, placeholder, value } = this.props;

    const showPlaceholder = value.length === 0;

    return (
      <div className="terminal" onClick={this.focusTextInput}>
        <input
          onChange={onChange}
          onKeyDown={this.handleKeyDown}
          placeholder={placeholder}
          ref={input => {
            this.input = input;
          }}
          type="text"
          value={value}
        />
        {showPlaceholder && (
          <span className="terminal__placeholder">{placeholder}</span>
        )}
        <span className="terminal__input">{value}</span>
      </div>
    );
  }
}

export default TerminalInput;
